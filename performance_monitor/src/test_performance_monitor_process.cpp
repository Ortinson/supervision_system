/*!*******************************************************************************************
 *  \file       performance_monitor_test_process.cpp
 *  \brief      PerformanceMonitorTest main file.
 *  \details    This file includes the PerformanceMonitorTest main declaration.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#include "test_performance_monitor.h"

//! This main do not follow the usual pattern to execute a DroneProcess class, due to its class nature.
int main(int argc, char **argv)
{
  PerformanceMonitorTest pMonitorTest (argc, argv);
  pMonitorTest.validate();
}
