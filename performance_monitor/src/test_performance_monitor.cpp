/*!*******************************************************************************************
 *  \file       performance_monitor_test.cpp
 *  \brief      PerformanceMonitorTest implementation file.
 *  \details    This file implements the PerformanceMonitorTest class.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/

 /*TODO
I can add an ip address at the roslaunch o try to obtain a true (ertting an IP known to work)
I can check for the actual supervised process to see if it is online or not
Do the same to know if it is started or not.
 */
#include "test_performance_monitor.h"
#include "drone_process.h"  //This file is needed to obtain the State and Error defs.

PerformanceMonitorTest::PerformanceMonitorTest(int argc, char **argv)
{
  printf("%s\n", "Starting PerformanceMonitorTest");
  ros::init(argc, argv, ros::this_node::getName());
  ros::NodeHandle n;
  
  watchdog_pub = n.advertise<droneMsgsROS::AliveSignal>("process_alive_signal", 10);
  error_pub = n.advertise<droneMsgsROS::ProcessError>("self_detected_process_error", 10);
  error_sub = n.subscribe("error_notification_topic", 1000, &PerformanceMonitorTest::errorInformerCallback,this);
  
  wifiIsOkClient = n.serviceClient<std_srvs::Trigger>("wifiIsOk");
  moduleIsOnlineClient = n.serviceClient<droneMsgsROS::askForModule>("moduleIsOnline");
  moduleIsStartedClient = n.serviceClient<droneMsgsROS::askForModule>("moduleIsStarted");

  all_ok = true;
  death_detected = false;
  node_restoration = false;
  node_error_detection = false;

  char buf[32];  
  gethostname(buf,sizeof buf);  
  hostname.append(buf);
}

PerformanceMonitorTest::~PerformanceMonitorTest()
{
  ROS_INFO("Closing PerformanceMonitorTest");
}

void PerformanceMonitorTest::validate()
{
  if(test1()) //Node death detection
    test2();  //Node reinsertion
  test3();    //Node error detection
  test4();    //moduleIsOnline service.
  test5();    //moduleIsStarted service.
  test6();    //wifiIsOk service.


  if(all_ok)
      printf("\n%s\n", COLOR_GREEN"All tests PASSED. PerformanceMonitor CHECKED."COLOR_RESET);
  else
    printf("\n%s\n", COLOR_RED"Performance_Monitor didn't passed all tests. check FAILED."COLOR_RESET);
  //string kill_command = "rosnode kill -a";
  //system(ping_command.c_str());
}

bool PerformanceMonitorTest::test1()
{
  ros::Duration(1).sleep();
  watchdog_message.current_state.state = DroneProcess::Started;
  watchdog_message.process_name = ros::this_node::getName();
  watchdog_message.hostname = hostname;
  watchdog_pub.publish(watchdog_message);

  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (death_detected)
      i=10;
  }
  if(death_detected)
    printf("%s%d%s\n",COLOR_GREEN "Test 1/",NUM_TEST,", Node death detection: PASSED" COLOR_RESET);
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 1/",NUM_TEST,", Node death detection: FAILED" COLOR_RESET);
    all_ok = false;
  }

};


void PerformanceMonitorTest::test2()
{
  watchdog_message.current_state.state = DroneProcess::Started;
  watchdog_message.process_name = ros::this_node::getName();
  watchdog_pub.publish(watchdog_message);

  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (node_restoration)
      i=10;
  }

  if(death_detected)
    printf("%s%d%s\n",COLOR_GREEN "Test 2/",NUM_TEST,", Node restoration: PASSED" COLOR_RESET);
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 2/",NUM_TEST,", Node restoration: FAILED" COLOR_RESET);
    all_ok = false;
  }

};

void PerformanceMonitorTest::test3()
{
  droneMsgsROS::ProcessError error_message;
  error_message.header.stamp = ros::Time::now();
  error_message.hostname = hostname;
  error_message.process_name = ros::this_node::getName();
  error_message.function = "fake function name";
  error_message.description = "This is a fake error used to test the performance monitor";
  error_message.reference_code = 3;
  error_pub.publish(error_message);

  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (node_error_detection)
      i=10;
  }

  if(node_error_detection)
    printf("%s%d%s\n",COLOR_GREEN "Test 3/",NUM_TEST,", Node error detection: PASSED" COLOR_RESET);
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 3/",NUM_TEST,", Node error detection: FAILED" COLOR_RESET);
    all_ok = false;
  }
};

void PerformanceMonitorTest::test4() // moduleIsOnline service.
{
  droneMsgsROS::askForModule srv;
  srv.request.module_name = "test_performance_monitor";
  bool subtestModuleIsOnline = false;
  bool subtestModuleIsOffline = false;
  death_detected = false;


  //Subtest 1
  if (moduleIsOnlineClient.call(srv)) //If call was ok.
  {
    if (srv.response.ack)  //If response is true.
    {
      subtestModuleIsOnline = true;
    }
  }
  else
  {
    printf("Can't make call on test 4");
  }

  //Subtest 2
  for (int i=1;i<10;i++)
  {
    ros::Duration(1).sleep();
    ros::spinOnce();
    if (death_detected)
      i=10;
  }
  if (moduleIsOnlineClient.call(srv)) //If call was ok.
  {
    if (!srv.response.ack)  //If response is false.
    {
      subtestModuleIsOffline = true;
    }
  }
  else
  {
    printf("Can't make call on test 4");
  }  

  if (subtestModuleIsOnline && subtestModuleIsOffline)
  {
      printf("%s%d%s\n",COLOR_GREEN "Test 4/",NUM_TEST,", ModuleIsOnline service: PASSED" COLOR_RESET);
  }
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 4/",NUM_TEST,", ModuleIsOnline service: FAILED" COLOR_RESET);
    all_ok = false;
  }
}

void PerformanceMonitorTest::test5() //moduleIsStarted service.
{
  droneMsgsROS::askForModule srv;
  srv.request.module_name = "test_performance_monitor";
  bool subtestModuleIsStarted = false;
  bool subtestModuleIsNotStarted = false;

  watchdog_message.current_state.state = DroneProcess::Started;
  watchdog_pub.publish(watchdog_message);

  //Subtest 1
  if (moduleIsStartedClient.call(srv)) //If call was ok.
  {
    if (srv.response.ack)  //If response is true.
    {
      subtestModuleIsStarted = true;
    }
  }
  else
  {
    printf("Can't make call on test 5");
  }

  //Subtest 2
  watchdog_message.current_state.state = DroneProcess::NotStarted;
  watchdog_pub.publish(watchdog_message);
  if (moduleIsStartedClient.call(srv)) //If call was ok.
  {
    if (!srv.response.ack)  //If response is false.
    {
      subtestModuleIsNotStarted = true;
    }
  }
  else
  {
    printf("Can't make call on test 5");
  }  

  if (subtestModuleIsStarted && subtestModuleIsNotStarted)
  {
      printf("%s%d%s\n",COLOR_GREEN "Test 5/",NUM_TEST,", ModuleIsStarted service: PASSED" COLOR_RESET);
  }
  else
  {
    printf("%s%d%s\n",COLOR_RED "Test 5/",NUM_TEST,", ModuleIsStarted service: FAILED" COLOR_RESET);
    all_ok = false;
  }
}

void PerformanceMonitorTest::test6() //wifiIsOk service.
{
  std_srvs::Trigger srv;
  if (wifiIsOkClient.call(srv)) //If call was ok.
  {
    printf(COLOR_ORANGE"%s%d, The wifiIsOk service response was %s \n"COLOR_RESET, "Test 6/",NUM_TEST,srv.response.success ? "true" : "false");
    printf(COLOR_ORANGE"Manually check if this is a valid response\n"COLOR_RESET);
  }
  else
  {
    printf("Can't make call on test 6");
  }

}

void PerformanceMonitorTest::errorInformerCallback(const droneMsgsROS::ProcessError::ConstPtr& msg)
{
  int code = msg->reference_code;
  
  if(code==1)  //If node death detected
    death_detected = true;
  else if (code==2)  //If node restoration occured
    node_restoration = true;
  else if (code==3)  //If node restoration occured
    node_error_detection = true;
}