/*!*******************************************************************************************
 *  \file       performance_monitor_test.h
 *  \brief      PerformanceMonitorTest definition file.
 *  \details    This file contains the PerformanceMonitorTest declaration. To obtain more information about
 *              it's definition consult the performance_monitor_test.cpp file.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <droneMsgsROS/AliveSignal.h>
#include <droneMsgsROS/ProcessError.h>
#include <droneMsgsROS/askForModule.h>
#include <std_srvs/Trigger.h>
#include <string>
#include <stdio.h>

 /*!******************************************************************************************
 *  \class      PerformanceMonitorTest
 *  \brief      This class has the required implementeation to check the performance monitor operation.
 *  \details    The PerformanceMonitorTest has three tests. The first and second one checks if 
 *              the performance monitor is able to detect a node death an its later restoration.
 *              The third test checks if the performance monitor is able to recieve and resend
 *              the errors detected by any node.\n
 *              All the test results are showed through std output.
 *********************************************************************************************/

#define COLOR_RED     "\x1b[31m" //!< Color code value in the terminal
#define COLOR_GREEN   "\x1b[32m" //!< Color code value in the terminal
#define COLOR_ORANGE  "\x1b[33m" //!< Color code value in the terminal
#define COLOR_RESET   "\x1b[0m"  //!< Color code value in the terminal
#define NUM_TEST 6               //!< Number of implemented tests at the moment 

class PerformanceMonitorTest
{
public:
  //! Constructor. \details The PerformanceMonitorTest constructor doesn't need any argument.
  PerformanceMonitorTest(int argc, char **argv);
  ~PerformanceMonitorTest();

  /*!*************************************************************************************
   * \details Validate is the function to be called after the object instantiation.
   * \return All the validation results are displayed throough stdout.
   ****************************************************************************************/
  void validate();

private:
  ros::Subscriber error_sub;                    //!< ROS publisher handler used to receive error messages
  ros::Publisher watchdog_pub;                  //!< ROS publisher handler used to send state messages.
  droneMsgsROS::AliveSignal watchdog_message;   //!< Message used in watchdog_pub topic 

  ros::Publisher error_pub;                     //!< ROS publisher handler used to send error messages.
  droneMsgsROS::ProcessError error_message;     //!< Message used in error_pub/error_sub topic

  ros::ServiceClient wifiIsOkClient;
  ros::ServiceClient moduleIsOnlineClient;
  ros::ServiceClient moduleIsStartedClient;

  bool all_ok;                           //!< Attribute storing if all tests run correctly or someone detects an errors
  bool death_detected;                   //!< Temporal attribute storing if a DroneProcess is alive or not
  bool node_restoration;                 //!< Temporal attribute storing if a DroneProcess is restoring or not
  bool node_error_detection;             //!< Temporal attribute storing if a DroneProcess has an error or not
  std::string hostname;                  //!< Attribute storing the computer hostname where runs the PerformanceMonitor


  /*!***************************************************************************************
   * \details The first test checks the performance monitor ability to detect a node death
   *          by sending the first alive message and waiting till the error is sent by the
   *          performance monitor.
   ****************************************************************************************/
  bool test1();

    /*!*************************************************************************************
   * \details The second test checks the performance monitor ability to reincorporate a 
   *          previously death node by sending a second alive message after a death error
   *          message is recieved.
   ****************************************************************************************/
  void test2();

  /*!*************************************************************************************
   * \details The third test checks if the supervisor resends an error message
   *          previously send by a node. 
   ****************************************************************************************/
  void test3();

  /*!*************************************************************************************
   * \details The fourth test checks the moduleIsOnline service.
   ****************************************************************************************/
  void test4();

  /*!*************************************************************************************
   * \details The fifth test checks the moduleIsStarted service.
   ****************************************************************************************/
  void test5();

  /*!*************************************************************************************
   * \details The sixth test checks the wifiIsOk service.
   ****************************************************************************************/
  void test6();

  /*!*************************************************************************************
   * \details The callback function is responsible of the error message obtainment.
   ****************************************************************************************/
  void errorInformerCallback(const droneMsgsROS::ProcessError::ConstPtr& msg);
};